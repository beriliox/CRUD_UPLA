-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-01-2017 a las 03:53:40
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `concentracion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `rut` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `dv` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `apellidom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image_perfil` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `rut`, `dv`, `nombre`, `apellidop`, `apellidom`, `direccion`, `ciudad`, `email`, `telefono`, `estado`, `pass`, `image_perfil`, `tipo_usuario`) VALUES
(1, '18161608', '3', 'ADMIN', 'MASTER', 'CONTROL', 'DHASDADASD', 'AHJSDGAJHSDGJH', 'ADMIN@GMAIL.COM', '73645263', 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/administrador.png', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `rut` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `dv` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidop` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidom` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promocion` int(11) DEFAULT NULL,
  `image_perfil` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `id_carrera` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`rut`, `dv`, `nombre`, `apellidop`, `apellidom`, `direccion`, `ciudad`, `email`, `telefono`, `promocion`, `image_perfil`, `id_carrera`, `estado`) VALUES
('10206103', '9', 'SUSPENDIDO', 'AGUILERIA', 'JERIA', 'asdada', 'asdasdasd', 'juan.a.aguilera@gmail.com', '48273645', 1232, 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 'NAF-1397', 'Suspendido'),
('10475515', '1', 'ELIMINADO', 'uytsduyatsduyastduyt', 'uytsauydtasuydtsauyd', 'sdaisdyiasdutasidasd 20w12312', 'gyausytuasytduaysdtu', 'dastdiasdhiady@sadyasiudyasihud', '', 2011, 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 'NAF-1397', 'Eliminado'),
('14621986', '1', 'PERLA', 'MAIKY', 'SAMI NEGRO', 'SAN MIGUEL', 'SAN ANTONIO', 'negro@maiky.cl', '', 2007, 'views/app/img/avatar_alumno.png', 'NAF-1397', 'Activo'),
('18161608', '3', 'CARLO ALBERTO', 'ECHEVERRIA', 'FUENTES ', 'SAN MIGUEL 360 PLACILLA', 'SAN ANTONIO', 'carlo.echeverria@alumnos.upla.cl', '42917068', 2011, 'html/Registros_alumnos/18161608/18161608.png', 'NAF-1397', 'Activo'),
('18230912', '5', 'JUAN FRANCISCO', 'PEREZ', 'CERDA ', 'PASAJE PISCIS 4603', 'VALPARAISO', 'juan.perez.c@alumnos.upla.cl', '65892456', 2010, 'html/Registros_alumnos/18230912/18230912.png', 'NAF-1397', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

CREATE TABLE `asignatura` (
  `cod_asign` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_asign` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_carrera` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `asignatura`
--

INSERT INTO `asignatura` (`cod_asign`, `nombre_asign`, `id_carrera`) VALUES
('CIF 01191-1', 'INTRODUCCION A LA INGENIERIA', 'NAF-1397'),
('CIF 01191-2', 'INTRODUCCION A LA INGENIERIA', 'NAF-1397'),
('CIF 01263-1', 'CALCULO DIFERENCIAL', 'NAF-1397'),
('CIF 01263-2', 'CALCULO DIFERENCIAL', 'NAF-1397'),
('CIF 01321-1', 'FISICA GENERAL I: MECANICA', 'NAF-1397'),
('CIF 01321-2', 'FISICA GENERAL I: MECANICA', 'NAF-1397'),
('CIF 01451-1', 'INTRODUCCION A LA INFORMATICA', 'NAF-1397'),
('CIF 01451-2', 'INTRODUCCION A LA INFORMATICA', 'NAF-1397'),
('CIF 01561-1', 'ALGEBRA', 'NAF-1397'),
('CIF 01561-2', 'ALGEBRA', 'NAF-1397'),
('CIF 02162-1', 'ALGEBRA LINEAL', 'NAF-1397'),
('CIF 02263-1', 'CALCULO INTEGRAL Y SERIES', 'NAF-1397'),
('CIF 02263-2', 'CALCULO INTEGRAL Y SERIES', 'NAF-1397'),
('CIF 02321-1', 'FISICA GENERAL II:CALOR Y ONDAS3', 'NAF-1397'),
('CIF 02321-2', 'FISICA GENERAL II:CALOR Y ONDAS', 'NAF-1397'),
('CIF 02452-1', 'FUNDAMENTOS DE PROGRAMACION', 'NAF-1397'),
('CIF 02452-2', 'FUNDAMENTOS DE PROGRAMACION', 'NAF-1397'),
('CIF 03263-1', 'CALCULO MULTIVARIADO', 'NAF-1397'),
('CIF 03321-1', 'ELECTROMAGNETISMO', 'NAF-1397'),
('CIF 03453-1', 'ESTRUCTURA Y REPRESENTACION DE DATOS', 'NAF-1397'),
('CIF 04263-1', 'ECUACIONES DIFERENCIALES', 'NAF-1397'),
('CIF 04321-1', 'FUNDAMENTOS DE ELECTRONICA', 'NAF-1397'),
('CIF 04452-1', 'TALLER DE PROGRAMACION', 'NAF-1397'),
('CIF 04556-1', 'SISTEMAS OPERATIVOS', 'NAF-1397'),
('CIF 04655-1', 'TEORIA DE SISTEMAS', 'NAF-1397'),
('CIF 05141-1', 'ESTADISTICA', 'NAF-1397'),
('CIF 05295-1', 'MACRO Y MICROECONOMIA', 'NAF-1397'),
('CIF 05353-1', 'MODELOS DE DATOS', 'NAF-1397'),
('CIF 05455-1', 'REDES DE COMPUTADORES', 'NAF-1397'),
('CIF 05555-1', 'ANALISIS DE SISTEMAS', 'NAF-1397'),
('CIF 06145-1', 'INVESTIGACION DE OPERACIONES', 'NAF-1397'),
('CIF 06297-1', 'GESTION DE EMPRESA', 'NAF-1397'),
('CIF 06356-1', 'SISTEMA DE BASE DE DATOS', 'NAF-1397'),
('CIF 06459-1', 'TECNOLOGIA DE MULTIMEDIA', 'NAF-1397'),
('CIF 06558-1', 'INGENIERIA DE SOFTWARE', 'NAF-1397'),
('CIF 06611-1', 'POLITICA Y ECONOMIA DE LA INFORMACION', 'NAF-1397'),
('CIF 07199-1', 'INFORMATICA Y DERECHO', 'NAF-1397'),
('CIF 07296-1', 'PLANIFICACION Y ESTRATEGICA', 'NAF-1397'),
('CIF 07359-1', 'INTELIGENCIA ARTIFICIAL', 'NAF-1397'),
('CIF 07458-1', 'METODOLOGIA DE DESARROLLO DE SOFTWARE', 'NAF-1397'),
('CIF 08197-1', 'FORMULACION Y EVALUACION DE PROYECTOS', 'NAF-1397'),
('CIF 08297-1', 'ADMINISTRACION DE RECURSOS HUMANOS', 'NAF-1397'),
('CIF 08399-1', 'SEMINARIO DE INVESTIGACION', 'NAF-1397'),
('CIF 08458-1', 'INGENIERIA DEL CONOCIMIENTO', 'NAF-1397'),
('CIF 08599-1', 'AUDITORIA INFORMATICA', 'NAF-1397'),
('CIF 09159-1', 'TALLER DE INTEGRADO', 'NAF-1397'),
('CIF 09341-1', 'INFORMATICA Y ETICA', 'NAF-1397'),
('CIF 33191-2', 'INTRODUCCION A LA INGENIERIAss', 'NAF-1397'),
('CIF 3836', 'ASIGNATURA PRUEBA', 'CIF 19999'),
('CIF 9299-1', 'PROYECTO DE TITULO I', 'NAF-1397'),
('EIF 07536-1', 'SOCIOLOGIA', 'NAF-1397'),
('FIF 05614-2', 'F.G.1.:ACONDICIONAMIENTO FISICO', 'NAF-1397'),
('HIF 02533-1', 'INGLES I', 'NAF-1397'),
('HIF 03111-1', 'INTRODUCCION A LAS CS. DE LA INFORMACION', 'NAF-1397'),
('HIF 03533-1', 'INGLES II', 'NAF-1397'),
('HIF 04113-1', 'FUENTES DE INFORMACION', 'NAF-1397'),
('IFF 07608-1', 'F.G.2: DESARROLLO ORGANIZACIONAL', 'NAF-1397'),
('IIF 05604-1', 'F.G.1.:DISEÃ‘O DE PAGINA WEB', 'NAF-1397'),
('IIF 05614-1', 'F.G.1.:PROGRAMACION EN NET', 'NAF-1397'),
('IIF 05626-1', 'F.G.1.:PROGRAMACION EN ANDROID', 'NAF-1397'),
('IIF 07630-1', 'F.G.2.: AYUDEMOS A LA REFORESTACION', 'NAF-1397');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `id_carrera` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_carrera` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_coordinador` int(11) NOT NULL,
  `id_departamento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`id_carrera`, `nombre_carrera`, `id_coordinador`, `id_departamento`) VALUES
('CIF 19999', 'CARRERA DE PRUEBA', 29, 1),
('NAF-1397', 'INGENIERIA EN INFORMATICA', 28, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador`
--

CREATE TABLE `coordinador` (
  `id` int(11) NOT NULL,
  `rut` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `dv` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `apellidom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image_perfil` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `coordinador`
--

INSERT INTO `coordinador` (`id`, `rut`, `dv`, `nombre`, `apellidop`, `apellidom`, `direccion`, `ciudad`, `email`, `telefono`, `estado`, `pass`, `image_perfil`, `tipo_usuario`) VALUES
(28, '14315550', '1', 'JAVIER ANDRES', 'CASTILLO', 'ALLARIA', 'UPLA', 'VALPARAISO', 'javier.castillo@upla.cl', '63582671', 'Activo', 'b58a7a0cb90a0ebae70f095a5c9d5925', 'views/app/img/avatar_alumno.png', 4),
(29, '10039557', '6', 'COORDINADOR', 'UPLA', 'ASGDASTD', 'TYASDGUYASGDY', 'TYGAYSDTASYDGUASD', 'COORDINADOR@GMAIL.COM', '111333', 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 4),
(30, '14601470', '4', 'YEAH', 'DASHDGSAJHG', 'JHGJSDHGASJHDGASD', 'AJSHDKAJSDHK', 'JHKAJSDHASKJD', 'DKAHGSDJS@UPLA.CL', '1223', 'Activo', '45e494879955fbbc0cad97d8885df8ff', 'views/app/img/avatar_alumno.png', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `decano`
--

CREATE TABLE `decano` (
  `id` int(11) NOT NULL,
  `rut` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `dv` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `apellidom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image_perfil` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `decano`
--

INSERT INTO `decano` (`id`, `rut`, `dv`, `nombre`, `apellidop`, `apellidom`, `direccion`, `ciudad`, `email`, `telefono`, `estado`, `pass`, `image_perfil`, `tipo_usuario`) VALUES
(1, '14601470', '4', 'DEC', 'DECANO', 'UPLA', 'JHKAJSDHAKJSDHASKJH', 'KJHASDASD', 'GHDKJHASJDHAKJSD@ASKDJHASKDJSHK', NULL, 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/avatar_alumno.png', 7),
(2, '18161608', '3', 'DECANO2', 'HSDGSHJ', 'HASDGAHJSDG', 'pppAAA', '123', 'DECANO@GMAIL.COM', '99191919', 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/avatar_alumno.png', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id` int(11) NOT NULL,
  `nombre_departamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_director` int(11) DEFAULT NULL,
  `id_facultad` int(11) DEFAULT NULL,
  `id_decano` int(11) DEFAULT NULL,
  `id_secretario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id`, `nombre_departamento`, `id_director`, `id_facultad`, `id_decano`, `id_secretario`) VALUES
(1, 'DEPTO. DISCIPLINARIO DE COMPUTACION E INFORMATICA', 1, 1, NULL, NULL),
(2, 'DEPTO', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `director_departamento`
--

CREATE TABLE `director_departamento` (
  `id` int(11) NOT NULL,
  `rut` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `dv` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `apellidom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image_perfil` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `director_departamento`
--

INSERT INTO `director_departamento` (`id`, `rut`, `dv`, `nombre`, `apellidop`, `apellidom`, `direccion`, `ciudad`, `email`, `telefono`, `estado`, `pass`, `image_perfil`, `tipo_usuario`) VALUES
(1, '17263172', '6', 'DIRECTOR', 'DIR', 'UPLA', 'dasdghfsdhgf', 'sdfashgdfashdgf', 'DIRECTOR@GMAIL.COM', '16516711', 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/avatar_alumno.png', 5),
(2, '14601470', '4', 'DIRECTOR', 'ACADEMICO', 'UPLA', 'JHGASDJHGASDJHG', 'HGASDJHAGSDJHG', 'HDGAJSHDGHJASD', NULL, 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/avatar_alumno.png', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `id` int(11) NOT NULL,
  `nombre_facultad` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_decano` int(11) DEFAULT NULL,
  `id_secretario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`id`, `nombre_facultad`, `id_decano`, `id_secretario`) VALUES
(1, 'FACULTAD DE INGENIERIA', 1, 1),
(2, 'FACU', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `id_inscripcion` int(11) NOT NULL,
  `rut` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cod_asign` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `periodo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oportunidad` int(11) DEFAULT NULL,
  `nota_final` float DEFAULT NULL,
  `estado` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `inscripcion`
--

INSERT INTO `inscripcion` (`id_inscripcion`, `rut`, `cod_asign`, `periodo`, `oportunidad`, `nota_final`, `estado`) VALUES
(1, '14621986', 'CIF 01191-1', '2011/2', 1, 4.5, 'Ter'),
(138, '18161608', 'CIF 01191-1', '2011/2', 1, 4.5, 'ter'),
(139, '18161608', 'CIF 01263-1', '2011/2', 1, 4.2, 'ter'),
(140, '18161608', 'CIF 01321-1', '2011/2', 1, 4, 'ter'),
(141, '18161608', 'CIF 01451-1', '2011/1', 1, 4.6, 'ter'),
(142, '18161608', 'CIF 01561-1', '2011/1', 1, 4.9, 'ter'),
(143, '18161608', 'CIF 02162-1', '2011/2', 1, 2, 'ter'),
(144, '18161608', 'CIF 02162-1', '2013/2', 2, 4.9, 'ter'),
(145, '18161608', 'CIF 02263-1', '2012/2', 1, 3.8, 'ter'),
(146, '18161608', 'CIF 02263-1', '2013/1', 2, 4.2, 'ter'),
(147, '18161608', 'CIF 02321-1', '2012/1', 1, 5, 'ter'),
(148, '18161608', 'CIF 02452-1', '2011/2', 1, 2.5, 'ter'),
(149, '18161608', 'CIF 02452-1', '2012/2', 2, 4, 'ter'),
(150, '18161608', 'HIF 02533-1', '2011/2', 1, 4.1, 'ter'),
(151, '18161608', 'HIF 03111-1', '2012/1', 1, 4.9, 'ter'),
(152, '18161608', 'CIF 03263-1', '2014/1', 1, 4.7, 'ter'),
(153, '18161608', 'CIF 03321-1', '2013/1', 1, 4.2, 'ter'),
(154, '18161608', 'CIF 03453-1', '2013/1', 1, 2.5, 'ter'),
(155, '18161608', 'CIF 03453-1', '2014/1', 2, 4.5, 'ter'),
(156, '18161608', 'HIF 03533-1', '2012/1', 1, 4.8, 'ter'),
(157, '18161608', 'HIF 04113-1', '2012/2', 1, 5.5, 'ter'),
(158, '18161608', 'CIF 04263-1', '2014/2', 1, 4.7, 'ter'),
(159, '18161608', 'CIF 04321-1', '2013/2', 1, 4.7, 'ter'),
(160, '18161608', 'CIF 04452-1', '2014/2', 1, 4.2, 'ter'),
(161, '18161608', 'CIF 04556-1', '2012/2', 1, 4, 'ter'),
(162, '18161608', 'CIF 04655-1', '2012/2', 1, 6, 'ter'),
(163, '18161608', 'CIF 05141-1', '2014/1', 1, 4.4, 'ter'),
(164, '18161608', 'CIF 05295-1', '2013/1', 1, 4.2, 'ter'),
(165, '18161608', 'CIF 05353-1', '2013/1', 1, 4, 'ter'),
(166, '18161608', 'CIF 05455-1', '2015/1', 1, 5.1, 'ter'),
(167, '18161608', 'CIF 05555-1', '2013/1', 1, 4.2, 'ter'),
(168, '18161608', 'FIF 05614-2', '2015/1', 1, 6.8, 'ter'),
(169, '18161608', 'IIF 05626-1', '2013/1', 1, 1, 'ter'),
(170, '18161608', 'CIF 06145-1', '2014/2', 1, 4.8, 'ter'),
(171, '18161608', 'CIF 06297-1', '2013/2', 1, 5.5, 'ter'),
(172, '18161608', 'CIF 06356-1', '2013/2', 1, 4.1, 'ter'),
(173, '18161608', 'CIF 06459-1', '2015/2', 1, 2.3, 'ter'),
(174, '18161608', 'CIF 06558-1', '2013/2', 1, 4, 'ter'),
(175, '18161608', 'CIF 06611-1', '2013/2', 1, 4.8, 'ter'),
(176, '18161608', 'CIF 07199-1', '2015/1', 1, 6.6, 'ter'),
(177, '18161608', 'CIF 07296-1', '2015/1', 1, 4.6, 'ter'),
(178, '18161608', 'CIF 07359-1', '2015/1', 1, 5.5, 'ter'),
(179, '18161608', 'CIF 07458-1', '2014/1', 1, 5.1, 'ter'),
(180, '18161608', 'EIF 07536-1', '2014/1', 1, 5.3, 'ter'),
(181, '18161608', 'CIF 08197-1', '2015/2', 1, 5.4, 'ter'),
(182, '18161608', 'CIF 08297-1', '2014/2', 1, 4.9, 'ter'),
(183, '18161608', 'CIF 08399-1', '2015/2', 1, 4.5, 'ter'),
(184, '18161608', 'CIF 08458-1', '2015/2', 1, 5.1, 'ter'),
(185, '18161608', 'CIF 08599-1', '2015/2', 1, 4.5, 'ter'),
(186, '18161608', 'CIF 09159-1', '2016/1', 1, 5.1, 'ter'),
(187, '18161608', 'CIF 09341-1', '2015/1', 1, 6.2, 'ter'),
(188, '18230912', 'CIF 01191-1', '2010/1', 1, 4, 'ter'),
(189, '18230912', 'CIF 01263-2', '2010/1', 1, 1.7, 'ter'),
(190, '18230912', 'CIF 01263-1', '2010/2', 2, 4, 'ter'),
(191, '18230912', 'CIF 01321-2', '2010/1', 1, 4.2, 'ter'),
(192, '18230912', 'CIF 01451-2', '2010/1', 1, 4.2, 'ter'),
(193, '18230912', 'CIF 01561-2', '2010/1', 1, 3.4, 'ter'),
(194, '18230912', 'CIF 01561-1', '2013/1', 2, 3.2, 'ter'),
(195, '18230912', 'CIF 01561-2', '2014/1', 3, 4.1, 'ter'),
(196, '18230912', 'CIF 02162-1', '2010/2', 1, 1.6, 'ter'),
(197, '18230912', 'CIF 02162-1', '2013/2', 2, 4.3, 'ter'),
(198, '18230912', 'CIF 02263-1', '2011/2', 1, 3.1, 'ter'),
(199, '18230912', 'CIF 02263-1', '2015/2', 2, 5.2, 'ter'),
(200, '18230912', 'CIF 02263-2', '2012/2', 2, 1, 'ter'),
(201, '18230912', 'CIF 02263-1', '2013/2', 3, 1.7, 'ter'),
(202, '18230912', 'CIF 02321-1', '2010/2', 1, 1.7, 'ter'),
(203, '18230912', 'CIF 02321-2', '2012/2', 2, 1.7, 'ter'),
(204, '18230912', 'CIF 02321-1', '2013/2', 3, 3.2, 'ter'),
(205, '18230912', 'CIF 02321-1', '2015/2', 4, 5.8, 'ter'),
(206, '18230912', 'CIF 02452-1', '2010/2', 1, 2.9, 'ter'),
(207, '18230912', 'CIF 02452-1', '2011/2', 2, 4.4, 'ter'),
(208, '18230912', 'HIF 02533-1', '2010/2', 1, 4.9, 'ter'),
(209, '18230912', 'HIF 03111-1', '2011/1', 1, 5.9, 'ter'),
(210, '18230912', 'CIF 03263-1', '2014/1', 1, 2.4, 'ter'),
(211, '18230912', 'CIF 03321-1', '2014/1', 1, 2, 'ter'),
(212, '18230912', 'CIF 03453-1', '2012/1', 1, 5.4, 'ter'),
(213, '18230912', 'HIF 03533-1', '2011/1', 1, 4.4, 'ter'),
(214, '18230912', 'HIF 04113-1', '2011/2', 1, 4.6, 'ter'),
(215, '18230912', 'CIF 04452-1', '2012/2', 1, 4.8, 'ter'),
(216, '18230912', 'CIF 04556-1', '2011/2', 1, 4, 'ter'),
(217, '18230912', 'CIF 04655-1', '2011/2', 1, 6.7, 'ter'),
(218, '18230912', 'CIF 05141-1', '2012/1', 1, 5.1, 'ter'),
(219, '18230912', 'CIF 05295-1', '2012/1', 1, 4.3, 'ter'),
(220, '18230912', 'CIF 05353-1', '2012/1', 1, 4.3, 'ter'),
(221, '18230912', 'CIF 05455-1', '2012/1', 1, 4, 'ter'),
(222, '18230912', 'CIF 05555-1', '2012/1', 1, 4.5, 'ter'),
(223, '18230912', 'CIF 06145-1', '2012/2', 1, 4.3, 'ter'),
(224, '18230912', 'CIF 06297-1', '2013/2', 1, 6.2, 'ter'),
(225, '18230912', 'CIF 06459-1', '2013/2', 1, 3.9, 'ter'),
(226, '18230912', 'CIF 06459-1', '2014/2', 2, 1, 'ter'),
(227, '18230912', 'CIF 06459-1', '2015/2', 3, 2.1, 'ter'),
(228, '18230912', 'CIF 06459-1', '2016/2', 4, 0, 'ins'),
(229, '18230912', 'CIF 06558-1', '2012/2', 1, 4.6, 'ter'),
(230, '18230912', 'CIF 07199-1', '2013/1', 1, 4.9, 'ter'),
(231, '18230912', 'CIF 07296-1', '2014/1', 1, 3.6, 'ter'),
(232, '18230912', 'CIF 07296-1', '2015/1', 2, 1.4, 'ter'),
(233, '18230912', 'CIF 07359-1', '2013/1', 1, 1, 'ter'),
(234, '18230912', 'CIF 07359-1', '2014/1', 2, 3.5, 'ter'),
(235, '18230912', 'CIF 07458-1', '2013/1', 1, 5.1, 'ter'),
(236, '18230912', 'EIF 07536-1', '2013/1', 1, 5.5, 'ter'),
(237, '18230912', 'IIF 07630-1', '2015/2', 1, 1, 'ter'),
(238, '18230912', 'CIF 08197-1', '2014/2', 1, 1, 'ter'),
(239, '18230912', 'CIF 08297-1', '2013/2', 1, 4.1, 'ter'),
(240, '18230912', 'CIF 08399-1', '2014/2', 1, 1, 'ter'),
(241, '18230912', 'CIF 08458-1', '2014/2', 1, 1, 'ter'),
(242, '18230912', 'CIF 08599-1', '2013/2', 1, 4.6, 'ter'),
(243, '18230912', 'CIF 09341-1', '2015/1', 1, 2.5, 'ter'),
(279, '18161608', 'CIF 06459-1', '2016/2', 2, 0, 'ins'),
(280, '18161608', 'CIF 9299-1', '2016/2', 1, 0, 'ins'),
(281, '18161608', 'IFF 07608-1', '2016/2', 1, 0, 'ins'),
(282, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(283, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(284, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(285, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(286, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(287, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(288, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(289, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(290, '18256317', 'CIF 01561-1', '2016/2', 2, NULL, 'ins'),
(291, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(292, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(293, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(294, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(295, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(296, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(297, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(298, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(299, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(300, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(301, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(302, '18256317', 'CIF 01191-2', '2016/1', 1, NULL, 'ins'),
(303, '14621986', 'CIF 33191-2', '2010/1', 3, 4, 'Ter'),
(304, '14621986', 'CIF 01191-2', '2010/1', 1, 4, 'Ter'),
(305, '14621986', 'CIF 01263-2', '2010/1', 1, 1.7, 'Ter'),
(306, '14621986', 'CIF 01263-1', '2010/2', 2, 4, 'Ter'),
(307, '14621986', 'CIF 01321-2', '2010/1', 1, 4.2, 'Ter'),
(308, '14621986', 'CIF 01451-2', '2010/1', 1, 4.2, 'Ter'),
(309, '14621986', 'CIF 01561-2', '2010/1', 1, 3.4, 'Ter'),
(310, '14621986', 'CIF 01561-1', '2013/1', 2, 3.2, 'Ter'),
(311, '14621986', 'CIF 01561-2', '2014/1', 3, 4.1, 'Ter'),
(312, '14621986', 'CIF 02162-1', '2010/2', 1, 1.6, 'Ter'),
(313, '14621986', 'CIF 02162-1', '2013/2', 2, 4.3, 'Ter'),
(314, '14621986', 'CIF 02263-1', '2011/2', 1, 3.1, 'Ter'),
(315, '14621986', 'CIF 02263-1', '2015/2', 2, 5.2, 'Ter'),
(316, '14621986', 'CIF 02263-2', '2012/2', 2, 1, 'Ter'),
(317, '14621986', 'CIF 02263-1', '2013/2', 3, 1.7, 'Ter'),
(318, '14621986', 'CIF 02321-1', '2010/2', 1, 1.7, 'Ter'),
(319, '14621986', 'CIF 02321-2', '2012/2', 2, 1.7, 'Ter'),
(320, '14621986', 'CIF 02321-1', '2013/2', 3, 3.2, 'Ter'),
(321, '14621986', 'CIF 02321-1', '2015/2', 4, 5.8, 'Ter'),
(322, '14621986', 'CIF 02452-1', '2010/2', 1, 2.9, 'Ter'),
(323, '14621986', 'CIF 02452-1', '2011/2', 2, 4.4, 'Ter'),
(324, '14621986', 'HIF 02533-1', '2010/2', 1, 4.9, 'Ter'),
(325, '14621986', 'HIF 03111-1', '2011/1', 1, 5.9, 'Ter'),
(326, '14621986', 'CIF 03263-1', '2014/1', 1, 2.4, 'Ter'),
(327, '14621986', 'CIF 03321-1', '2014/1', 1, 2, 'Ter'),
(328, '14621986', 'CIF 03453-1', '2012/1', 1, 5.4, 'Ter'),
(329, '14621986', 'HIF 03533-1', '2011/1', 1, 4.4, 'Ter'),
(330, '14621986', 'HIF 04113-1', '2011/2', 1, 4.6, 'Ter'),
(331, '14621986', 'CIF 04452-1', '2012/2', 1, 4.8, 'Ter'),
(332, '14621986', 'CIF 04556-1', '2011/2', 1, 4, 'Ter'),
(333, '14621986', 'CIF 04655-1', '2011/2', 1, 6.7, 'Ter'),
(334, '14621986', 'CIF 05141-1', '2012/1', 1, 5.1, 'Ter'),
(335, '14621986', 'CIF 05295-1', '2012/1', 1, 4.3, 'Ter'),
(336, '14621986', 'CIF 05353-1', '2012/1', 1, 4.3, 'Ter'),
(337, '14621986', 'CIF 05455-1', '2012/1', 1, 4, 'Ter'),
(338, '14621986', 'CIF 05555-1', '2012/1', 1, 4.5, 'Ter'),
(339, '14621986', 'IIF 05604-1', '2013/1', 1, 5.1, 'Ter'),
(340, '14621986', 'IIF 05614-1', '2012/1', 1, 1, 'Ter'),
(341, '14621986', 'CIF 06145-1', '2012/2', 1, 4.3, 'Ter'),
(342, '14621986', 'CIF 06297-1', '2013/2', 1, 6.2, 'Ter'),
(343, '14621986', 'CIF 06356-1', '2012/2', 1, 4.8, 'Ter'),
(344, '14621986', 'CIF 06459-1', '2013/2', 1, 3.9, 'Ter'),
(345, '14621986', 'CIF 06459-1', '2014/2', 2, 1, 'Ter'),
(346, '14621986', 'CIF 06459-1', '2015/2', 3, 5.3, 'Ter'),
(347, '14621986', 'CIF 06459-1', '2016/2', 4, 0, 'Ins'),
(348, '14621986', 'CIF 06558-1', '2012/2', 1, 4.6, 'Ter'),
(349, '14621986', 'CIF 06611-1', '2012/2', 1, 4.8, 'Ter'),
(350, '14621986', 'CIF 07199-1', '2013/1', 1, 4.9, 'Ter'),
(351, '14621986', 'CIF 07296-1', '2014/1', 1, 3.6, 'Ter'),
(352, '14621986', 'CIF 07296-1', '2015/1', 2, 1.4, 'Ter'),
(353, '14621986', 'CIF 07359-1', '2013/1', 1, 1, 'Ter'),
(354, '14621986', 'CIF 07359-1', '2014/1', 2, 3.5, 'Ter'),
(355, '14621986', 'CIF 07458-1', '2013/1', 1, 5.1, 'Ter'),
(356, '14621986', 'EIF 07536-1', '2013/1', 1, 5.5, 'Ter'),
(357, '14621986', 'IIF 07630-1', '2015/2', 1, 0, 'Ter'),
(358, '14621986', 'CIF 08197-1', '2014/2', 1, 1, 'Ter'),
(359, '14621986', 'CIF 08297-1', '2013/2', 1, 4.1, 'Ter'),
(360, '14621986', 'CIF 08399-1', '2014/2', 1, 1, 'Ter'),
(361, '14621986', 'CIF 08458-1', '2014/2', 1, 1, 'Ter'),
(362, '14621986', 'CIF 08599-1', '2013/2', 1, 4.6, 'Ter'),
(363, '14621986', 'CIF 09341-1', '2015/1', 1, 2.5, 'Ter');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `id` int(11) NOT NULL,
  `rut` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `dv` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `apellidom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image_perfil` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`id`, `rut`, `dv`, `nombre`, `apellidop`, `apellidom`, `direccion`, `ciudad`, `email`, `telefono`, `estado`, `pass`, `image_perfil`, `tipo_usuario`) VALUES
(1, '10416809', '4', 'profesor', 'prof', 'DJHASGDJHGAS', 'SGDHJASGDJHAG', 'GSDJHASGDJHASG', 'dasdsa@sdasdsad', '', 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/avatar_alumno.png', 3),
(13, '14315550', '1', 'JAVIER ANDRES', 'CASTILLO', 'ALLARIA', 'UPLA', 'VALPO', 'javier.castillo@upla.cl', '51236512', 'Activo', '5773d2fb902cb8144d02a0d0377c09e7', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3),
(14, '12042904', '3', 'MANUEL', 'CONTRERAS', 'GOMEZ', 'hgsdasdhty', 'tuysgdaysdtgy', 'dgasjdsajdg', '43264293', 'Activo', '901b6b7d2c4287c43727c3c111f6bd95', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3),
(15, '14010712', '3', 'ENRIQUE', 'VARGAS', 'FUENTES', 'YTDUYFGSYFTU', 'YGTDSUYDSTF', 'DTHASYDTUASYDTDS@DFKHSTDUYFTU', '52165310', 'Activo', 'af0e209f020155ec1a586da0d3b0ea26', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3),
(16, '12541072', '3', 'MIGUEL', 'RUBIO', 'ROMAN', 'TDFIUDHSUH', 'UDFHSIUHYIU', 'JKASBDHGJASGBH@SDKFHSDHF', '1122', 'Activo', 'b95146f8b14f7da0cb29c3c923f021ea', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3),
(17, '11290397', '6', 'TATIANA', 'ILABACA', 'WENTELEMN', 'UJSDGJASHBD', 'GJHBDAHSGJH', 'GDJHGASJDHSYT', '', 'Activo', '47b4d0c9445131dec646a489805f0f52', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3),
(18, '13385094', '5', 'JOSE', 'MEZA', 'RAMIREZ', 'UYDSIHYIU', 'YQISUDA', 'FDHDSFYSDUFY@DFKDUIHYI', '', 'Activo', 'b95146f8b14f7da0cb29c3c923f021ea', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3),
(19, '13526724', '4', 'HECTOR', 'LUNA', 'CARRASCO', 'IUDFHUSDFHUH', 'UDFHSUSDFHUDFSY', 'FHASHDYASUD@SKFJYSDIFUY', '', 'Activo', '47b4d0c9445131dec646a489805f0f52', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3),
(21, '13560172', '1', 'PROFESOR', 'ADASD', 'SADASDASD', 'JHJKASDHAKJSDH', 'JHSADKJSHDAKJSD', 'PROFESOR@GMAIL.COM', '', 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'https://www.b1g1.com/assets/admin/images/no_image_user.png', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prof_asignatura`
--

CREATE TABLE `prof_asignatura` (
  `id` int(11) NOT NULL,
  `cod_asign` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_profesor` int(11) DEFAULT NULL,
  `periodo` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `prof_asignatura`
--

INSERT INTO `prof_asignatura` (`id`, `cod_asign`, `id_profesor`, `periodo`) VALUES
(1, 'CIF 01321-1', 0, '2013/1'),
(85, 'CIF 02162-1', 15, '2013/2'),
(86, 'CIF 06356-1', 17, '2013/2'),
(87, 'CIF 06459-1', 13, '2015/2'),
(88, 'CIF 07359-1', 16, '2015/1'),
(89, 'CIF 02263-1', 19, '2013/1'),
(90, 'CIF 06297-1', 18, '2013/2'),
(91, 'CIF 01191-1', 14, '2011/2'),
(112, 'CIF 04263-1', 15, '2014/2'),
(113, 'CIF 06459-1', 13, '2016/2'),
(114, 'CIF 01191-1', 22, ''),
(115, 'CIF 01191-1', 22, ''),
(116, 'CIF 01191-1', 22, ''),
(117, 'CIF 01191-1', 22, ''),
(118, 'CIF 01191-1', 22, ''),
(119, 'CIF 01191-1', 22, ''),
(120, 'CIF 01191-1', 22, ''),
(121, 'CIF 01191-1', 22, ''),
(122, 'CIF 01191-1', 22, ''),
(123, 'CIF 01191-1', 22, ''),
(124, 'CIF 01191-1', 22, ''),
(125, 'CIF 01191-1', 22, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secretario_academico`
--

CREATE TABLE `secretario_academico` (
  `id` int(11) NOT NULL,
  `rut` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `dv` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `apellidom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image_perfil` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `secretario_academico`
--

INSERT INTO `secretario_academico` (`id`, `rut`, `dv`, `nombre`, `apellidop`, `apellidom`, `direccion`, `ciudad`, `email`, `telefono`, `estado`, `pass`, `image_perfil`, `tipo_usuario`) VALUES
(1, '18161608', '3', 'SECRETARIO', 'ASDJHASHJDG', 'JHGASJHDGASJHDG', 'GJHSDGAJSHDGJAHSGJ', 'HGJSAHDGAJHSGD', 'AGSDJHAGSDHJ@SADKJHASDHH', '', 'Activo', '45e494879955fbbc0cad97d8885df8ff', 'views/app/img/avatar_alumno.png', 6),
(2, '17352635', '3', 'SECRETARIO2', 'SJDHAKJSDHKJ', 'HHDKJASKJADAKJSH', 'KJHASKJDHAKSJDHASKJDHKJ', 'HKHASDKJAHDK', 'SECRETARIO@GMAIL.COM', '79999998', 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/avatar_alumno.png', 6),
(3, '14601470', '4', 'SECR', 'UPLA', 'JHDKJASHDKJH', 'SHDGAJHSGDAJSHDG', 'JHGSJDHGASJD', 'SECR@GMAIL.COM', NULL, 'Activo', 'cfcd208495d565ef66e7dff9f98764da', 'views/app/img/avatar_alumno.png', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `tipo_usuario` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `tipo_usuario`) VALUES
(2, 'Administrador'),
(3, 'Profesor'),
(4, 'Coordinador'),
(5, 'Director Departament'),
(6, 'Secretario Academico'),
(7, 'Decano');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rut` (`rut`),
  ADD KEY `tipo_usuario` (`tipo_usuario`);

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`rut`),
  ADD KEY `id_carrera` (`id_carrera`);

--
-- Indices de la tabla `asignatura`
--
ALTER TABLE `asignatura`
  ADD PRIMARY KEY (`cod_asign`),
  ADD KEY `asignaturaadasd_ibfk_1` (`id_carrera`);

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`id_carrera`),
  ADD KEY `id_coordinador` (`id_coordinador`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- Indices de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rut` (`rut`),
  ADD KEY `tipo_usuario` (`tipo_usuario`);

--
-- Indices de la tabla `decano`
--
ALTER TABLE `decano`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_usuario` (`tipo_usuario`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_director` (`id_director`),
  ADD KEY `id_facultad` (`id_facultad`),
  ADD KEY `id_decano` (`id_decano`),
  ADD KEY `id_secretario` (`id_secretario`) USING BTREE;

--
-- Indices de la tabla `director_departamento`
--
ALTER TABLE `director_departamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_usuario` (`tipo_usuario`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`id_inscripcion`),
  ADD KEY `rut` (`rut`),
  ADD KEY `cod_asign` (`cod_asign`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rut` (`rut`),
  ADD KEY `tipo_usuario` (`tipo_usuario`);

--
-- Indices de la tabla `prof_asignatura`
--
ALTER TABLE `prof_asignatura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cod_asign` (`cod_asign`),
  ADD KEY `id_profesor` (`id_profesor`);

--
-- Indices de la tabla `secretario_academico`
--
ALTER TABLE `secretario_academico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_usuario` (`tipo_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `decano`
--
ALTER TABLE `decano`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `director_departamento`
--
ALTER TABLE `director_departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `facultad`
--
ALTER TABLE `facultad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  MODIFY `id_inscripcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=364;
--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `prof_asignatura`
--
ALTER TABLE `prof_asignatura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT de la tabla `secretario_academico`
--
ALTER TABLE `secretario_academico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD CONSTRAINT `administrador_ibfk_1` FOREIGN KEY (`tipo_usuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD CONSTRAINT `carrera_ibfk_1` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`);

--
-- Filtros para la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD CONSTRAINT `coordinador_ibfk_1` FOREIGN KEY (`tipo_usuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `decano`
--
ALTER TABLE `decano`
  ADD CONSTRAINT `decano_ibfk_1` FOREIGN KEY (`tipo_usuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`id_director`) REFERENCES `director_departamento` (`id`),
  ADD CONSTRAINT `departamento_ibfk_2` FOREIGN KEY (`id_facultad`) REFERENCES `facultad` (`id`),
  ADD CONSTRAINT `departamento_ibfk_3` FOREIGN KEY (`id_decano`) REFERENCES `decano` (`id`),
  ADD CONSTRAINT `departamento_ibfk_4` FOREIGN KEY (`id_secretario`) REFERENCES `secretario_academico` (`id`);

--
-- Filtros para la tabla `director_departamento`
--
ALTER TABLE `director_departamento`
  ADD CONSTRAINT `director_departamento_ibfk_1` FOREIGN KEY (`tipo_usuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD CONSTRAINT `profesor_ibfk_1` FOREIGN KEY (`tipo_usuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `secretario_academico`
--
ALTER TABLE `secretario_academico`
  ADD CONSTRAINT `secretario_academico_ibfk_1` FOREIGN KEY (`tipo_usuario`) REFERENCES `usuarios` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

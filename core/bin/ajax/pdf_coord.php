<?php

include('../../../core/models/coneccion.php');
include('../../../FPDF/fpdf.php');



$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', '', 10);
$pdf->Image('../../../views/app/img/logo_upla.png' , 10 ,8, 70 , 20,'PNG');
$pdf->Cell(85, 10, '', 0);
$pdf->Cell(65, 10, 'CRUD UPLA', 0);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(40, 10, 'Fecha:  '.date('d-m-Y').'', 0);
$pdf->Ln(20);
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(75, 8, '', 0);
$pdf->Cell(150, 0, 'LISTA DE COORDINADORES', 0);
$pdf->Ln(10);


$pdf->SetFont('Arial', 'B', 6);
$pdf->Cell(45, 8, 'Nombre', 0);
$pdf->Cell(20, 8, 'RUT', 0);
$pdf->Cell(40, 8, 'Email.', 0);
$pdf->Cell(29, 8, 'Telefono', 0);
$pdf->Cell(33, 8, 'Direccion.', 0);


$pdf->Ln(8);
$pdf->SetFont('Arial', '', 5);
//CONSULTA
$alumnos = mysql_query("SELECT * FROM Coordinador ORDER BY apellidop DESC");


while($alumno = mysql_fetch_array($alumnos)){
  $pdf->Cell(45, 8, $alumno['nombre'] . ' ' . $alumno['apellidop'] . ' ' . $alumno['apellidom'], 0);
  $pdf->Cell(20, 8, $alumno['rut'], 0);
  $pdf->Cell(40, 8, $alumno['email'], 0);
  $pdf->Cell(29, 8, $alumno['telefono'], 0);
  $pdf->Cell(33, 8, $alumno['direccion'] . ' - ' . $alumno['ciudad'], 0);

	$pdf->Ln(8);
}

$pdf->Output('Lista_Coordinadores.pdf','D');
?>

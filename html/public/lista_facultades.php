<?php
  if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_coord']) or isset($_SESSION['app_id_prof']) or isset($_SESSION['app_id_direc']) or isset($_SESSION['app_id_secr']) or isset($_SESSION['app_id_dec'])) {

  } else{
    header('location: ?view=index');

  }
?>
<?php include('html/overall/header.php'); ?>

<body>


<?php include('html/overall/topnav.php');
?>
<legend><h3 class="col-lg-offset-5">Listado de Facultades</h3></legend>

<div class="row">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="input-group">
              <input type="text" class="form-control" placeholder="Busca Facultad por Nombre o Secretario" id="bs-prod_fac">
              <span class="input-group-btn">
                <a class="buscar_facultades btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></a>
              </span>
        </div>
      </div>
</div>
</br>

<div class="table-responsive">
<table class="table">
  <thead class="thead-inverse">
    <tr class="oculto">
      <th>Nombre Facultad</th>
      <th>Secretario Académico</th>
      <th>Decano</th>

      <?php

        if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_coord'])) { //admin Y Coord

          echo '<th>Acción</th>';

        }
      ?>
    </tr>
  </thead>
  <tbody>
    <div id="agrega-registros_facultades"></div>

    <?php

    include('core/models/coneccion.php');

    $consulta=mysql_query("SELECT f.id, f.nombre_facultad as nombre_facultad, s.nombre as nombre_secretario, s.apellidop as apellidop_s,
                                  d.nombre as nombre_decano, d.apellidop as apellidop_d
                           FROM Facultad f, Secretario_Academico s, Decano d
                           WHERE f.id_secretario=s.id AND f.id_decano=d.id",$link);

    while($deptos = mysql_fetch_assoc($consulta)) {
    #while($deptos = $resultado->fetch_array(MYSQLI_BOTH)) {
      echo '<tr class="oculto">';
      echo '<td>' . $deptos['nombre_facultad']. '</td>';
      echo '<td>' . $deptos['nombre_secretario'] . ' ' .$deptos['apellidop_s']. '</td>';
      echo '<td>' . $deptos['nombre_decano'] . ' ' .$deptos['apellidop_d']. '</td>';
        if(isset($_SESSION['app_id'])) { //admin y Coord
          echo '<td>' . '<a id="',$deptos['id'],'" class="update_facultades btn btn-success"><i class="fa fa-repeat"></i> Actualizar </a>' . '</td>';
        }
        echo '</tr>';

    }

    ?>

  </tbody>
</table>
</div>
</br></br></br></br>

<?php include('html/overall/footer.php'); ?>

</body>
</html>

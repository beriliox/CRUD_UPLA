<?php
  if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_coord']) or isset($_SESSION['app_id_prof']) or isset($_SESSION['app_id_direc']) or isset($_SESSION['app_id_secr']) or isset($_SESSION['app_id_dec'])) {

  } else{
    header('location: ?view=index');

  }
?>
<?php include('html/overall/header.php'); ?>

<body>


<?php include('html/overall/topnav.php');
?>
<legend><h3 class="col-lg-offset-5">Listado de Departamentos</h3></legend>

<div class="row">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="input-group">
              <input type="text" class="form-control" placeholder="Busca Departamento por Nombre o Director o Facultad" id="bs-prod_depto">
              <span class="input-group-btn">
                <a class="buscar_departamentos btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></a>
              </span>
        </div>
      </div>
</div>
</br>

<div class="table-responsive">
<table class="table">
  <thead class="thead-inverse">
    <tr class="oculto">
      <th>Nombre Departamento</th>
      <th>Director</th>
      <th>Facultad</th>

      <?php

        if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_coord'])) { //admin Y Coord

          echo '<th>Acción</th>';

        }
      ?>
    </tr>
  </thead>
  <tbody>
    <div id="agrega-registros_departamentos"></div>

    <?php

    include('core/models/coneccion.php');

    $consulta=mysql_query("SELECT d.id, d.nombre_departamento as nombre_departamento, dir.nombre as nombre, dir.apellidop as apellidop_dir,
                                  f.nombre_facultad as nombre_facultad
                           FROM   Departamento d, Director_Departamento dir, Facultad f
                           WHERE  d.id_director=dir.id AND d.id_facultad=f.id",$link);

    while($deptos = mysql_fetch_assoc($consulta)) {
    #while($deptos = $resultado->fetch_array(MYSQLI_BOTH)) {
      echo '<tr class="oculto">';
      echo '<td>' . $deptos['nombre_departamento']. '</td>';
      echo '<td>' . $deptos['nombre']. ' ' . $deptos['apellidop_dir'] .'</td>';
      echo '<td>' . $deptos['nombre_facultad'] .'</td>';
        if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_secr']) or isset($_SESSION['app_id_dec'])) { //admin y Coord
          echo '<td>' . '<a id="',$deptos['id'],'" class="update_departamentos btn btn-success"><i class="fa fa-repeat"></i> Actualizar </a>' . '</td>';
        }
        echo '</tr>';

    }

    ?>

  </tbody>
</table>
</div>
</br></br></br></br>

<?php include('html/overall/footer.php'); ?>

</body>
</html>

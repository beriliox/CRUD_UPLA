<?php
  if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_coord']) or isset($_SESSION['app_id_prof']) or isset($_SESSION['app_id_direc']) or isset($_SESSION['app_id_secr']) or isset($_SESSION['app_id_dec'])) {

  } else{
    header('location: ?view=index');

  }
?>
<?php include('html/overall/header.php'); ?>



<?php include('html/overall/topnav.php'); ?>

<?php

  include('core/models/coneccion.php');



  if($_GET['id']) {
  	$id = $_GET['id'];
  	$rut = mysql_escape_String($id);
    $sql=mysql_query("SELECT * FROM Director_Departamento WHERE id = '$id'", $link);

    while($directores = mysql_fetch_assoc($sql)) {

      echo '<form id="formid" class="form-horizontal">
        <fieldset>
          <center><legend><h3>Ficha del Director</h3></legend>
          <div class="form-group">
              <img src="',$directores['image_perfil'],'" alt="" class="img-responsive  img-circle" style="width:240px; height:250px;"/>
          </div></center>
          <div class="form-group">
            <label for="inputNombres" class="col-lg-2 control-label col-lg-offset-2">Nombres</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputNombres_dir" name="nombres" placeholder="Nombres" maxlength="50" value="',$directores['nombre'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputApellidoP" class="col-lg-2 control-label col-lg-offset-2">Apellido Paterno</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputApellidoP_dir" name ="apellidop" placeholder="Apellido Paterno" maxlength="20" value="',$directores['apellidop'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputApellidoM" class="col-lg-2 control-label col-lg-offset-2">Apellido Materno</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputApellidoM_dir" name="apellidom" placeholder="Apellido Materno" maxlength="20" value="',$directores['apellidom'],'" disabled="disabled">
            </div>
          </div>
          <label for="inputRut" class="col-lg-2 control-label col-lg-offset-2">RUT</label>
          <div class="form-group">
            <div class="col-lg-4 col-md-10 col-xs-9">
              <input type="text" class="form-control rut" id="inputRut_dir" name="rut" placeholder="RUT" maxlength="8" value="',$directores['rut'],'" disabled="disabled">
            </div>
            <div class="col-lg-1 col-md-2 col-xs-3">
              <input type="text" class="form-control dv" id="inputDigVer_dir" name="dv" placeholder="Dig. Ver." maxlength="1" value="',$directores['dv'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputCorreo" class="col-lg-2 control-label col-lg-offset-2">Correo</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputCorreo_dir" name="correo" placeholder="Correo" maxlength="50" value="',$directores['email'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputCorreo" class="col-lg-2 control-label col-lg-offset-2">Telefono (+56 9)</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputTelefono_dir" name="telefodno" placeholder="Telefono" maxlength="8" value="',$directores['telefono'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputDireccion" class="col-lg-2 control-label col-lg-offset-2">Dirección</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputDireccion_dir" name="dir" placeholder="Dirección" maxlength="60" value="',$directores['direccion'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputCiudad" class="col-lg-2 control-label col-lg-offset-2">Ciudad</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputCiudad_dir" name="ciudad" placeholder="Ciudad" maxlength="50" value="',$directores['ciudad'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputurl_foto" class="col-lg-2 control-label col-lg-offset-2">URL Foto</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="inputurl_foto_dir" name="url_foto" placeholder="URL Foto" maxlength="200" value="',$directores['image_perfil'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPass" class="col-lg-2 control-label col-lg-offset-2">Contraseña</label>
            <div class="col-lg-5">
              <input type="password" class="form-control" id="input_Pass_dir" name="pass" placeholder="Contraseña" maxlength="20" value="',$directores['pass'],'" disabled="disabled">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEstado" class="col-lg-2 control-label col-lg-offset-2">Estado</label>
            <div class="col-lg-5">
              <select class="form-control" id="inputEstado_dir" name="estado" maxlength="10" disabled="disabled">';

              echo '<option>'. $directores['estado']. '</option>';

              $consulta=mysql_query("SELECT DISTINCT estado FROM Alumno",$link);

              while($estado = mysql_fetch_assoc($consulta)) {
                if(($directores['estado']!=$estado['estado']) and $estado['estado']!= ''){
                  echo '<option>'. $estado['estado']. '</option>';
                }
              }


              echo '</select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-4">
              <a class="btn btn-primary" href="?view=directores">Lista de Directores</a>';
              if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_secr']) or isset($_SESSION['app_id_dec'])){
                echo '<a id="',$directores['id'],'" class="update_c_director btn btn-success col-lg-offset-1 col-md-offset-1 col-xs-offset-1"><span class="glyphicon glyphicon-off"></span> Actualizar Datos</a>';
                echo '<button id="habilitar_director" class="btn btn-default btn-success col-lg-offset-1 col-md-offset-1 col-xs-offset-1"><span class="glyphicon glyphicon-off"></span> Actualizar Datos</button>';
              }
      echo '</div>

          </div>
        </fieldset>
      </form>';
    }
  }
  echo '</br></br></br></br>'
?>

<?php include('html/overall/footer.php'); ?>

<?php
  if(isset($_SESSION['app_id']) or isset($_SESSION['app_id_coord'])) {

  } else{
    header('location: ?view=index');

  }
?>
<?php include('html/overall/header.php'); ?>

<body>


<?php include('html/overall/topnav.php');

?>
<script type="text/javascript">


  function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));




}

var nombreimg = getParameterByName('rut');
  document.cookie = 'rutimagen='+nombreimg;
</script>


<legend><h3 style="text-align:center;">Modificar Imagen de Perfil</h3></legend>
<center> <div role="form"><div class="form-group">
                 <label for="inputFoto">Capturar Foto Perfil con Camara Web</label><br>
                 <video id="video"></video>
                 <button id="startbutton">Tomar Foto</button>
                 <canvas id="canvas"></canvas>
                 <br>
                 <a href="#" class="button" id="download" download="prueba.png">Descargar</a>
                 <button id="bt-save">Subir Imagen</button>
                 <script type="text/javascript">
                   function escribir(){
                      var nombreimg = getParameterByName('rut');
                      document.getElementById('myimage').src='html/Registros_alumnos/'+nombreimg+'/'+nombreimg+'.png';
                      document.cookie = 'rutimagen='+nombreimg;
                      $('#interface').show();
                      $('#myimage').show();
                      $('#cortar').show();
                   }
                 </script>
                 <button onclick="escribir()">recargar</button>

                 <div id='interface' style="display: none;" ><img id="myimage" src="snapshots/" ></div>




                 <form action='save.php' id='myform' method='POST'>
                   <input type='hidden' name='crop-x' id='crop-x' value='0' />
                   <input type='hidden' name='crop-y' id='crop-y' value='0' />
                   <input type='hidden' name='crop-w' id='crop-w' value='0' />
                   <input type='hidden' name='crop-h' id='crop-h' value='0' />
                   <input id ="cortar" type='submit' value='Cortar Imagen' style="display: none;" />
                 </form>
             </div></center>
             </div>

             <br>
             <br>
             <strong><center>Ó tambien seleccionar imagen desde su PC</center></strong>
             <center><form action="subir_imagen.php" method="POST" enctype="multipart/form-data">
             	<label>Subir Imagen de perfil</label>
             	<input type="file" name="imagenperfil" id="imagenperfil">
              <input type="submit" name="enviar" value="Subir Imagen">

             </form></center>


<script>


(function() {

  //Empezamos por tomar los elementos HTML que necesitamos y definimos el ancho (width) del video a 320 y la altura (height) a 0, ya que calcularemos la altura apropiada posteriormente.

  var streaming = false,
      video        = document.querySelector('#video'),
      canvas       = document.querySelector('#canvas'),
      photo        = document.querySelector('#photo'),
      startbutton  = document.querySelector('#startbutton'),
      width = 200,
      height = 0;


      //Ahora necesitamos obtener el video desde la cámara web. Actualmente esto está predeterminado para los diferentes navegadores, así que necesitamos comprobar cuál es compatible:

  navigator.getMedia = ( navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia ||
                         navigator.msGetUserMedia);


    //Le solicitamos al navegador que nos dé un video sin audio y obtenemos una secuencia (stream) en la función de retrollamada:
  navigator.getMedia(
    {
      video: true,
      audio: false
    },
    function(stream) {
      if (navigator.mozGetUserMedia) {
        video.mozSrcObject = stream;
      } else {
        var vendorURL = window.URL || window.webkitURL;
        video.src = vendorURL.createObjectURL(stream);
      }
      video.play();
    },
    function(err) {
      console.log("An error occured! " + err);
    }
  );

  /*Luego necesitamos configurar el tamaño del video a las dimensiones deseadas.
  Nos subscribimos al evento canplay del video y leemos sus dimensiones utilizando videoHeight y videoWidth. Estas no están disponible realmente hasta que el evento sea iniciado. Establecemos streaming a verdadero (true) para que compruebe esto solo una vez, mientras que el evento canplay  siga en actividad.

  Esto es todo lo que se necesita para que inicie el video.*/

  video.addEventListener('canplay', function(ev){
    if (!streaming) {
      height = video.videoHeight / (video.videoWidth/width);
      video.setAttribute('width', width);
      video.setAttribute('height', height);
      canvas.setAttribute('width', width);
      canvas.setAttribute('height', height);
      streaming = true;
    }
  }, false);

  /* En esta función, reestablecemos el tamaño del lienzo (canvas) a las dimensiones del video, el cual lo sustituye y tenemos un marco del video, el cual se copia al canvas. Luego necesitamos convertir los datos del canvas en datos tipo URL con un encabezado PNG, y establecer el src de la fotografía a este mismo url. */

  function takepicture() {
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(video, 0, 0, width, height);
    var data = canvas.toDataURL('image/png');
    //data = data.replace("image/png", "image/octet-stream");
    //document.location.href = data;
    photo.setAttribute('src', data);
  }

  /*Ahora necesitamos capturar una imagen utilizando un lienzo (canvas). Asignamos un manejador de eventos al botón de inicio para llamar a la función de takepicture.*/

  startbutton.addEventListener('click', function(ev){
      takepicture();
    ev.preventDefault();
  }, false);


  // con el siguiente codigo desencadenamos el evento para descargar el archivo


  var button = document.getElementById('download');
button.addEventListener('click', function (e) {
    var dataURL = canvas.toDataURL('image/png');
    button.href = dataURL;
});

})();
</script>

<script type="text/javascript">
  jQuery(function($){
  var d = document, ge = 'getElementById';

  $('#interface').on('cropmove cropend',function(e,s,c){
    d[ge]('crop-x').value = c.x;
    d[ge]('crop-y').value = c.y;
    d[ge]('crop-w').value = c.w;
    d[ge]('crop-h').value = c.h;
  });

  // Most basic attachment example
  $('#myimage').Jcrop({
    setSelect: [ 175, 100, 400, 300 ]
  });

  $('#text-inputs').on('change','input',function(e){
    $('#myimage').Jcrop('api').animateTo([
      parseInt(d[ge]('crop-x').value),
      parseInt(d[ge]('crop-y').value),
      parseInt(d[ge]('crop-w').value),
      parseInt(d[ge]('crop-h').value)
    ]);
  });

});
</script>
